﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace WcfFormulaCalc
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IRPN" in both code and config file together.
    [ServiceContract]
    public interface IRPN
    {
        [OperationContract]
        bool IsDelimeter(char c);

        [OperationContract]
        bool IsOperator(char с);

        [OperationContract]
        byte GetPriority(char s);

        [OperationContract]
        string GetExpression(string input);

        [OperationContract]
        double Counting(string input);

        [OperationContract]
        double Calculate(string input);
        
    }
}
