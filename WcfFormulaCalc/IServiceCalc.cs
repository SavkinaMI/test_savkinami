﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using WcfFormulaCalc;

namespace WcfFormulaCalc
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IService1" in both code and config file together.
    [ServiceContract]
    public interface IServiceCalc
    {

        [OperationContract]
        string GetData(int value);

        [OperationContract]
        CompositeType GetDataUsingDataContract(CompositeType composite);

        // проверка соединения
        [OperationContract]
        string TestConnection();

        [OperationContract]
        string AddCollection(string name,string value);

        [OperationContract]
        void GetCollection(List<Formula> list);

        [OperationContract]
        string GetValueDetails(string F);

        [OperationContract]
        bool ListElement(string F);


        
    }
    
    [DataContract]
    public class CompositeType
    {
        bool boolValue = true;
        string stringValue = "Hello ";

        [DataMember]
        public bool BoolValue
        {
            get { return boolValue; }
            set { boolValue = value; }
        }

        [DataMember]
        public string StringValue
        {
            get { return stringValue; }
            set { stringValue = value; }
        }
    }
    [DataContract]
    [KnownType(typeof(Formula))]
    public class ListCollection
    {
        [DataMember]
        public List<Formula> SpisokFormul { get; set; }

    }
    
    [DataContract]
    public class Formula
    {
        [DataMember]
        public string Name { get; set; }
        [DataMember]
        public string Value { get; set; }

        public override string ToString()
        {
            return string.Format("{0}={1}", Name, Value);
        }
    }

}
