﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using System.Net;


namespace WcfFormulaCalc
{
    [DataContract]
    [System.ServiceModel.Activation.AspNetCompatibilityRequirements(RequirementsMode = System.ServiceModel.Activation.AspNetCompatibilityRequirementsMode.Required)]
    public class ServiceCalc : IServiceCalc
    {

        private static List<Formula> GlobalList = new List<Formula>();

        public string GetData(int value)
        {
            return string.Format("You entered: {0}", value);
        }
  

        public string TestConnection()
        {
            return "OK";
        }


        public string  AddCollection(string name,string value)
        {
           var obj = GlobalList.Find(item => item.Name == name) ;

           if (obj == null) //Если в списке
           {
               GlobalList.Add(new Formula() { Name = name, Value = value });
               WebOperationContext.Current.OutgoingResponse.StatusCode = HttpStatusCode.Created;
               return "Добавлено определение для: " + name + " \n\n";
           }
           //else  GlobalList.RemoveAll(item => item.Name == name);

           else return "Значение уже определено\n\n";       
        }
            
        public void GetCollection(List<Formula> List)
        {
            foreach (Formula obj in GlobalList)
            {
                List.Add(new Formula() { Name = obj.Name, Value = obj.Value });
               // List.Add(obj);
               // Console.WriteLine(obj.ToString());
            }
        }
      //проверка на наличие переменной в списке
        public bool ListElement(string F)
        {
            var obj = GlobalList.Find(item => item.Name == F) ;
                
             if ( obj != null )  return true;
            else return false;

        }

      //Получить значение переменной
        public string GetValueDetails(string F)
        {
            try
            {
                var obj = GlobalList.Find(item => item.Name == F);
                return obj.Value.ToString();
             
            }
            catch (Exception ex)
            {
                throw new FaultException<string>
                        (ex.Message);
            }

        }
        public CompositeType GetDataUsingDataContract(CompositeType composite)
        {
            if (composite == null)
            {
                throw new ArgumentNullException("composite");
            }
            if (composite.BoolValue)
            {
                composite.StringValue += "Suffix";
            }
            return composite;

        }

        

            
    }
}
