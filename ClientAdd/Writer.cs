﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClientAdd
{
    public class Writer
    {

        private ConsoleColor ok;     // цвет сообщений для успешных операций
        private ConsoleColor error;   // цвет сообщений об ошибках
        private ConsoleColor pred;     // цвет сообщений-предупреждений



        public ConsoleColor okColor { get { return ok; } set { ok = value; } }

        public ConsoleColor ErrorColor { get { return error; } set { error = value; } }

        public ConsoleColor PredColor { get { return pred; } set { pred = value; } }

       public void ConsoleWriter()
        {
            ok = ConsoleColor.Green;
            error = ConsoleColor.Red;
            pred = ConsoleColor.Blue;


        }
       public void Write(ConsoleColor color, string value)
       {
           ConsoleColor oldColor = Console.ForegroundColor;
           Console.ForegroundColor = color;
           Console.Write(value);
           Console.ForegroundColor = oldColor;
       }


    }
}
