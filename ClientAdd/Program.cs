﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ServiceModel;
using ClientAdd.ServiceReference1;
using ClientAdd.ServiceRef_RPN;



namespace ClientAdd
{
    class Program
    {
        static void Main(string[] args)
        {
            ServiceCalcClient service = new ServiceCalcClient();
            RPNClient serviceRPN = new RPNClient();

           // ListCollection List = new ListCollection();
            List<Formula> ListAdd = new List<Formula>();
            List<Formula> ListGet = new List<Formula>();
            Writer wr = new Writer();
             try
            {

            Console.WriteLine("Соединение с сервисом...\n");
            
            if (!string.Equals(service.TestConnection(), "OK", StringComparison.InvariantCultureIgnoreCase))
            {
                throw new Exception("Соединение не установлено");
            }

                 Console.WriteLine("Заполнение массива формул. Для выхода введите Exit");
                 string F = "";
           
                while (F.ToLower() != "exit")
                {
                    Console.WriteLine("Введите Формулу:");
                    F = Console.ReadLine();
                    
                    if (F.Contains("="))
                    {
                         
                        string name = F.Substring(0, F.IndexOf("=")).Replace(" ", "");
                        string value = F.Substring(F.IndexOf("=") + 1).Replace(" ", "");


                        //Проверка формулы
                        if (name.Length == 1)  //длина переменной
                        {
                            if (!serviceRPN.IsOperator(value[0])) //строка начинается с буквы или разделителя
                            {
                                for (int i = 1; i < value.Length; i++) //Для каждого символа в строке
                                {
                                    if (i+1 < value.Length)
                                    {
                                        if (Char.IsLetter(value[i]) && Char.IsLetter(value[i + 1]))
                                    {
                                    wr.Write(ConsoleColor.Yellow, "Имя каждой переменной должно содержать не более одного символа.\n\n");
                                    goto begin;
                                    }

                                    else continue;
                          
                                    }    
                                    
                                }
                                
                            }
                            else 
                             {
                              wr.Write(ConsoleColor.Yellow, "Имя каждой переменной должно начинаться с буквы или разделителя.\n\n");
                              goto begin;
                             }
                           
                            wr.Write(ConsoleColor.Green, service.AddCollection(name, value)); 
                           
                        begin: continue;
                        }
                        else
                        {
                            wr.Write(ConsoleColor.Yellow, "Имя переменной должно содержать не более одного символа.\n\n");
                        }

                    }
                }
                
               
            }
            catch (Exception e)
            {
                wr.Write(ConsoleColor.Red, "Error: " + e.Message);
            }
        }
        }
    
}
