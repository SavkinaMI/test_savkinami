﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ClientGet.ServiceReference1 {
    
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ServiceModel.ServiceContractAttribute(ConfigurationName="ServiceReference1.IRPN")]
    public interface IRPN {
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IRPN/IsDelimeter", ReplyAction="http://tempuri.org/IRPN/IsDelimeterResponse")]
        bool IsDelimeter(char c);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IRPN/IsDelimeter", ReplyAction="http://tempuri.org/IRPN/IsDelimeterResponse")]
        System.Threading.Tasks.Task<bool> IsDelimeterAsync(char c);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IRPN/IsOperator", ReplyAction="http://tempuri.org/IRPN/IsOperatorResponse")]
        bool IsOperator(char с);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IRPN/IsOperator", ReplyAction="http://tempuri.org/IRPN/IsOperatorResponse")]
        System.Threading.Tasks.Task<bool> IsOperatorAsync(char с);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IRPN/GetPriority", ReplyAction="http://tempuri.org/IRPN/GetPriorityResponse")]
        byte GetPriority(char s);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IRPN/GetPriority", ReplyAction="http://tempuri.org/IRPN/GetPriorityResponse")]
        System.Threading.Tasks.Task<byte> GetPriorityAsync(char s);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IRPN/GetExpression", ReplyAction="http://tempuri.org/IRPN/GetExpressionResponse")]
        string GetExpression(string input);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IRPN/GetExpression", ReplyAction="http://tempuri.org/IRPN/GetExpressionResponse")]
        System.Threading.Tasks.Task<string> GetExpressionAsync(string input);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IRPN/Counting", ReplyAction="http://tempuri.org/IRPN/CountingResponse")]
        double Counting(string input);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IRPN/Counting", ReplyAction="http://tempuri.org/IRPN/CountingResponse")]
        System.Threading.Tasks.Task<double> CountingAsync(string input);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IRPN/Calculate", ReplyAction="http://tempuri.org/IRPN/CalculateResponse")]
        double Calculate(string input);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IRPN/Calculate", ReplyAction="http://tempuri.org/IRPN/CalculateResponse")]
        System.Threading.Tasks.Task<double> CalculateAsync(string input);
    }
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public interface IRPNChannel : ClientGet.ServiceReference1.IRPN, System.ServiceModel.IClientChannel {
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public partial class RPNClient : System.ServiceModel.ClientBase<ClientGet.ServiceReference1.IRPN>, ClientGet.ServiceReference1.IRPN {
        
        public RPNClient() {
        }
        
        public RPNClient(string endpointConfigurationName) : 
                base(endpointConfigurationName) {
        }
        
        public RPNClient(string endpointConfigurationName, string remoteAddress) : 
                base(endpointConfigurationName, remoteAddress) {
        }
        
        public RPNClient(string endpointConfigurationName, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(endpointConfigurationName, remoteAddress) {
        }
        
        public RPNClient(System.ServiceModel.Channels.Binding binding, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(binding, remoteAddress) {
        }
        
        public bool IsDelimeter(char c) {
            return base.Channel.IsDelimeter(c);
        }
        
        public System.Threading.Tasks.Task<bool> IsDelimeterAsync(char c) {
            return base.Channel.IsDelimeterAsync(c);
        }
        
        public bool IsOperator(char с) {
            return base.Channel.IsOperator(с);
        }
        
        public System.Threading.Tasks.Task<bool> IsOperatorAsync(char с) {
            return base.Channel.IsOperatorAsync(с);
        }
        
        public byte GetPriority(char s) {
            return base.Channel.GetPriority(s);
        }
        
        public System.Threading.Tasks.Task<byte> GetPriorityAsync(char s) {
            return base.Channel.GetPriorityAsync(s);
        }
        
        public string GetExpression(string input) {
            return base.Channel.GetExpression(input);
        }
        
        public System.Threading.Tasks.Task<string> GetExpressionAsync(string input) {
            return base.Channel.GetExpressionAsync(input);
        }
        
        public double Counting(string input) {
            return base.Channel.Counting(input);
        }
        
        public System.Threading.Tasks.Task<double> CountingAsync(string input) {
            return base.Channel.CountingAsync(input);
        }
        
        public double Calculate(string input) {
            return base.Channel.Calculate(input);
        }
        
        public System.Threading.Tasks.Task<double> CalculateAsync(string input) {
            return base.Channel.CalculateAsync(input);
        }
    }
}
