﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ServiceModel;
using ClientGet.ServiceReference2;
using ClientGet.ServiceReference1;

namespace ClientGet
{
    class Program
    {
        static void Main(string[] args)
        {
            ServiceCalcClient service = new ServiceCalcClient();
            RPNClient serviceRPN = new RPNClient();
            Writer wr = new Writer();

            try
            {
                // проверка соединения                
               Console.WriteLine("Соединение с сервисом... ");
                if (!string.Equals(service.TestConnection(), "OK", StringComparison.InvariantCultureIgnoreCase))                
                {                    
                    throw new Exception("Соединение не установлено");                 
                }

                Console.WriteLine("Для выхода введите Exit \n");
               
                string str = "";

                while (str.ToLower() != "exit")
                {

                Console.WriteLine("Введите значение переменной: ");

                str = Console.ReadLine().ToString();

                //проверка на наличие в коллекции
                if (service.ListElement(str) == true)
                {
                  //  Console.WriteLine(str+" = " + service.GetValueDetails(str)+ "\n");
                    string op = service.GetValueDetails(str);

                    for (int i = 0; i < op.Length; i++) //Для каждого символа в входной строке
                    {
                        if (serviceRPN.IsDelimeter(op[i]) || serviceRPN.IsOperator(op[i]) || Char.IsDigit(op[i]))
                            continue;
                        //если символ - буква, ищем в списке формул

                        else if (service.ListElement(op[i].ToString()) == true)
                        {
                            op = op.Replace(op[i].ToString(), service.GetValueDetails(op[i].ToString()));

                        }
                        else if (service.ListElement(op[i].ToString()) == false)
                        {
                          //  Writer.Write( , "Неизвестная переменная " + op[i] + "\n");

                            wr.Write(ConsoleColor.Yellow, "Неизвестная переменная " + op[i] + "\n");
                            goto begin;
                        }
                    }

                    wr.Write(ConsoleColor.Green, "Результат: " + serviceRPN.Calculate(str + " = " + op)); //Считываем, и выводим результат
                begin: continue;
                }
                else wr.Write(ConsoleColor.Yellow, "Неизвестная переменная " + str + "\n");
                
                }
            }
            
            catch (Exception e)
            {
                wr.Write(ConsoleColor.Red,  "Error: " + e.Message);
            }
            

        }
        
    }
}
